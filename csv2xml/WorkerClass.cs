﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;

namespace csv2xml
{
    class WorkerClass
    {
        #region private members fields

        private Dictionary<string, int> fields;

        #endregion

        #region public properties
        public string CsvDelim { get; set; }

        public char CsvQuote { get; set; }

        public char CsvEscape { get; set; }

        public string CsvLinebreak { get; set; }

        public string Pos_start { get; set; } = "<##pos_start##>";

        public String pos_ende { get; set; } = "<##pos_ende##>";

        #endregion


        public WorkerClass(string CsvDelim = ";", char CsvQuote = '"', char CsvEscape = '"', string CsvLinebreak = "\r\n")
        {
            this.CsvDelim = CsvDelim;
            this.CsvQuote = CsvQuote;
            this.CsvEscape = CsvEscape;
            this.CsvLinebreak = CsvLinebreak;
        }

        public void ConvertFile(String templname, String filename, String output = "")
        {
            var template = File.ReadAllText(templname);

            if (String.IsNullOrEmpty(output))
            {
                // ggfs .csv entfernen, aber immer .xml hinten dran hängen
                output = filename.Replace(".csv", "") + ".xml";
            }

            var result = template.Substring(0, template.IndexOf(Pos_start)) + template.Substring(template.IndexOf(pos_ende));
            
            var position = template.Substring(template.IndexOf(Pos_start) + Pos_start.Length, template.IndexOf(pos_ende) - template.IndexOf(Pos_start) - Pos_start.Length);

            var body = new StringBuilder();

            using (var reader = new CsvReader(new StreamReader(filename), configuration: GetCsvConfig()))
            {
                reader.Read();

                reader.ReadHeader();
                
                var headline = reader.HeaderRecord;

                fields = new Dictionary<String, int>();

                var i = 0;

                foreach (var fld in headline)
                {
                    fields.Add(fld, i++);
                }

                var lineCount = 0;

                while (reader.Read())
                {
                    //var line = reader.ReadLine();
                    //
                    //var values = line.Split(CsvDelim);

                    

                    var values = new List<string>();

                    for (var ind = 0; ind < reader.Context.Parser.Record.Length; ind++)
                    {
                        values.Add(reader.Context.Parser.Record[ind]);
                    }

                    int start = 0;

                    // check first run
                    if (body.Length < 1)
                    {
                        start = -1;
                        while ((start = result.IndexOf("<##", start + 1)) > 0)
                        {
                            var retrn = ReplacePlaceholders(ref result, values.ToArray());
                        }

                    }

                    var temppos = position;

                    var ret = ReplacePlaceholders(ref temppos, values.ToArray());

                    if (String.IsNullOrEmpty(ret))
                    {
                        body.Append(temppos);
                    }
                    else
                    {
                        ret += $"(line {lineCount + 1})";
                        WritetoErrorLog(filename, ret);
                    }

                    lineCount++;
                }

            }

            File.WriteAllText(output, result.Replace(pos_ende, body.ToString()));

        }

        protected CsvConfiguration GetCsvConfig()
        {
            return new CsvHelper.Configuration.CsvConfiguration(CultureInfo.CurrentCulture) { 
                HasHeaderRecord = true,
                NewLine = CsvLinebreak,
                Delimiter = CsvDelim,
                Escape = CsvEscape,
                Quote = CsvQuote
            };
        }

        public static void WritetoErrorLog(string filename, string ret)
        {
            throw new NotImplementedException();
        }

        protected String ReplacePlaceholders(ref String text, String[] values, bool filterSpecial = false)
        {
            int start = 0;

            while ((start = text.IndexOf("<##")) > 0)
            {
                var placeholder = text.Substring(start, text.IndexOf("##>") + 3 - start);

                var chunk = placeholder.Substring(3, placeholder.Length - 6);

                // filter special chunks
                if (!filterSpecial || !placeholder.Equals(Pos_start) && !placeholder.Equals(pos_ende))
                {
                    if (fields.ContainsKey(chunk))
                    {
                        // is chunk in field list AND is contains value list that index
                        if (values.Length >= fields[chunk])
                        {
                            text = text.Replace(placeholder, values[fields[chunk]]); ;
                        }
                        else
                        {
                            return $"## {chunk} NOT FOUND in CSV line ";
                        }
                    }
                    else
                    {
                        return "## CSV line is not complete ";
                    }
                }
            }

            return String.Empty;
        }
    }
}
