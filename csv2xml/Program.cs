﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace csv2xml
{
    class Program
    {

        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("");
                Console.WriteLine("Syntax: csv2xml <csvdatei.csv> [<template_andere.xml>] ");
                Console.WriteLine("");
                Console.Write("Taste drücken...");
                Console.ReadKey();
                return 1;
            }

            if (!File.Exists(args[0]))
            {
                Console.WriteLine("FEHLER: Eingabe-Datei ist nicht da oder Zugriff nicht gewährt!");
                return 1;
            }

            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "template.xml") || (args.Length > 1 && !File.Exists(args[1])))
            {
                Console.WriteLine("FEHLER: template not found");
                return 1;
            }

            var worker = new WorkerClass();

            if (args.Length == 1)
            {
                worker.ConvertFile(AppDomain.CurrentDomain.BaseDirectory + "template.xml", args[0]);
            } else
            {
                worker.ConvertFile(args[1], args[0]);
            }

            return 0;
        }
    }
}
